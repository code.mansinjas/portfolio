/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
          {
            protocol: 'https',
            hostname: 'mandeep7303.vercel.app',
            port: '',
            pathname: '/_next/image?**',
          },
        ],
      },
};

export default nextConfig;
