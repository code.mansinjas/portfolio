import { ThemeContextProvider } from "@/context/ThemeContext";
import Footer from "@/components/Footer/Footer";
import Header from "@/components/Header/Header";
import ThemeSwitcher from "@/components/ThemeSwitcher";

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <div className="main-container">
        <Header />
        <ThemeSwitcher />
        {children}
        <Footer />
      </div>
    </>
  );
}
