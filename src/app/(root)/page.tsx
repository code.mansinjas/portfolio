import React from "react";
import "./homepage.css";
import { GiHand } from "react-icons/gi";
import SectionTitle from "@/components/SectionTitle/SectionTitle";
import TechSection from "@/sections/TechSection/TechSection";
import ProjectSection from "@/sections/ProjectSection/ProjectSection";
import AboutMeSection from "@/sections/AboutMeSection/AboutMeSection";
import Image from "next/image";


const page = () => {
  return (
    <>
      <div className="intro-section section-container">
        <div className="inner-container">
          <div className="container-left dev-avatar">
            <Image
              src={'/avatar-bw.webp'}
              alt=""
              width={300}
              height={300}
            />
          </div>
          <div className="container-right dev-intro-text">
            <h1>
              Back-End Node <br />
              Developer <GiHand />{" "}
            </h1>
            <p>
              Hi, I'm Mandeep Singh. A passionate Back-end Node Developer based
              in Ulhasnagar, Mumbai.
            </p>
          </div>
        </div>
      </div>
      <div className="tech section-container">
        <SectionTitle text="Tech Stack" />
        <TechSection />
      </div>
      <div className="project section-container">
        <SectionTitle text="Projects" />
        <ProjectSection />
      </div>
      <div className="aboutus section-container">
        <SectionTitle text="About Me" />
        <AboutMeSection />
      </div>
    </>
  );
};

export default page;
