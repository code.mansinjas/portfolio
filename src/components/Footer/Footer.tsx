import React from 'react'
import './footer.css'
import SocialShare from '../SocialShareIcons/SocialShare'
import Link from 'next/link'

const Footer = () => {
  return (
    <footer className="footer">
        <div className="copyright-container">
            created by: <span><Link href={`/`}>MCode</Link></span>
        </div>
        <SocialShare/>
    </footer>
  )
}

export default Footer