import React, { useContext } from "react";
import "./header.css";
import Link from "next/link";
import SocialShare from "../SocialShareIcons/SocialShare";
import { useGlobalTheme } from "@/context/ThemeContext";

const Header = () => {
  // const theme = useGlobalTheme()
  return (
    <>
      <header className="header">
        <div className="navbar">
          <SocialShare />
          <div className="logo-container">
            <Link href={`/`}>Mandeep</Link>
          </div>
          <div className="nav-list">
            <div className="nav-item">
              <Link href={"/"}>Home</Link>
            </div>
            <div className="nav-item">
              <Link href={"/"}>Contact</Link>
            </div>
            <div className="nav-item">
              <Link href={"/"}>About Me</Link>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
