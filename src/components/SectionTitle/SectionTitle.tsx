import React, { FC } from "react";
import "./sectionTitle.css";

interface SectionTitleInterface {
  text: string;
}

const SectionTitle: FC<SectionTitleInterface> = ({
  text = "Sample Text",
}: SectionTitleInterface) => {
  return (
    <div className="title-container">
      <h2 className="main-title">{text}</h2>
    </div>
  );
};

export default SectionTitle;
