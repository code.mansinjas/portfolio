import React from "react";
import './socialshare.css'
import { FaPhone } from "react-icons/fa6";
import { BsWhatsapp } from "react-icons/bs";
import { BsFacebook } from "react-icons/bs";
import { FiInstagram } from "react-icons/fi";
import { FiGithub } from "react-icons/fi";

const SocialShare = () => {
  return (
    <div className="social-container">
      <div className="social-item">
        <FaPhone size={25} />
      </div>
      <div className="social-item">
        <BsWhatsapp size={25} />
      </div>
      <div className="social-item">
        <BsFacebook size={25} />
      </div>
      <div className="social-item">
        <FiInstagram size={25} />
      </div>
      <div className="social-item">
        <FiGithub size={25} />
      </div>
    </div>
  );
};

export default SocialShare;
