import React from "react";

const ThemeSwitcher = () => {
  return (
    <div className="theme-selector">
      <div className="theme-div">
      <button
        className="theme-button"
        // onClick={SetBWTheme()}
      >{`Switch to Color Mode`}</button>
      </div>
    </div>
  );
};

export default ThemeSwitcher;
