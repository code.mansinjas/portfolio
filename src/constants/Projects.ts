import { StackType } from "./TechStack"

interface ProjectInterface {
    thumbImg: string
    title: string
    stack: Array<StackType>
}

const project: Array<ProjectInterface> = [
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    },
    {
        thumbImg: '/sample-thumbnail.webp',
        title: 'A Sample text for a Porfolio Website which we are looking at.',
        stack: ['NextJS']
    }
]

export default project