interface AboutMeInterface {
    [key: string]: Array<{
        title: string
        subText?: string
    }>
}

const aboutMeData: AboutMeInterface = {
    "Education": [
        {
            title: "Master of Science in Information Technology (MSC.I.T), 2022",
            subText: "Guru Nanak Khalsa College, Matunga, Mumbai, Maharashtra"
        },
        {
            title: "Bachelor of Science in Information Technology (BSC.I.T), 2020",
            subText: "R.K.Talreja College, Ulhasnagar, Thane, Maharashtra"
        },
        {
            title: "Higher Secondary School (HSC), 2017",
            subText: "Guru Nanak Khalsa College, Matunga, Mumbai, Maharashtra"
        }
    ],
    "Skills": [
        {
            title: "Certification in CEH",
            subText: ""
        }
    ]
}

export default aboutMeData