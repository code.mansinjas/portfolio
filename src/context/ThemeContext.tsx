"use client";
import { createContext, useContext, useState } from "react";

type ThemeContextInterface = {
  BWTheme: boolean;
  // SetBWTheme: any;
}

export const ThemeContext = createContext<ThemeContextInterface>({
  BWTheme: true,
  // SetBWTheme: () => {},
});

export const ThemeContextProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [BWTheme, SetBWThemeState] = useState(true);
  const SetBWTheme = () => SetBWThemeState(!BWTheme);
  return (
    <ThemeContext.Provider value={{ BWTheme: true }}>
      {children}
    </ThemeContext.Provider>
  );
};

export function useGlobalTheme() {
  return useContext(ThemeContext);
}
