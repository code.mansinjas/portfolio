"use client";
import React, { useState } from "react";
import "./aboutmeSection.css";
import aboutMeData from "@/constants/aboutMe";

const AboutMeSection = () => {
  const [displayIndex, SetDisplayIndex] = useState(0);
  return (
    <div className="aboutme ">
      <div className="inner-aboutme-con">
        <div className="left-aboutme">
          {Object.keys(aboutMeData).map((key, index) => (
            <h4
              className={`degree-text shadow ${
                displayIndex == index ? "active" : ""
              }`}
              onClick={() => SetDisplayIndex(index)}
            >
              {key}
            </h4>
          ))}
        </div>
        <div className="right-aboutme">
          {Object.entries(aboutMeData).map(([key, value], index) => {
            return (
              <div
                className={`main-data ${displayIndex == index ? "active" : ""}`}
              >
                <h4 className="title-name">{key}</h4>
                {value.map((ele) => {
                  return (
                    <div className="detail">
                      <h5>{ele?.title || ""}</h5>
                      <p>{ele?.subText || ""}</p>
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default AboutMeSection;
