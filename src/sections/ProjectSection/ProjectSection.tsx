import React from "react";
import "./projectSection.css";
import project from "@/constants/Projects";
import Image from "next/image";

const ProjectSection = () => {
  return (
    <div className="main-section project-section">
      <div className="project-inner-div">
        {project.map((project) => (
          <ProjectTile {...project} />
        ))}
      </div>
    </div>
  );
};

interface ProjectTile {
  title: string;
  thumbImg: string;
}
const ProjectTile = ({ thumbImg, title }: ProjectTile) => {
  return (
    <a className="project-tile">
      <Image src={thumbImg} alt="" fill />
      <h3 className="project-title">{title}</h3>
    </a>
  );
};

export default ProjectSection;
