"use client";
import React from "react";
import './techSection.css';
import { FaReact } from "react-icons/fa";
import { FaHtml5 } from "react-icons/fa";
import { IoLogoCss3 } from "react-icons/io";
import { IoLogoJavascript } from "react-icons/io";
import { FaNodeJs } from "react-icons/fa";
import { FaDocker } from "react-icons/fa";
import { SiKubernetes } from "react-icons/si";
import { FaGithub } from "react-icons/fa";
import Link from "next/link";

const TechSection = () => {
  return (
    <>
      <div className="main-section tech-section">
        <div>
        <Link href={`/`}><FaNodeJs size={100} /></Link>
        <Link href={`/`}><FaReact size={100} /></Link>
        <Link href={`/`}><FaHtml5 size={100} /></Link>
        <Link href={`/`}><IoLogoCss3 size={100} /></Link>
        <Link href={`/`}><IoLogoJavascript size={100} /></Link>
        <Link href={`/`}><FaDocker size={100} /></Link>
        <Link href={`/`}><SiKubernetes size={100} /></Link>
        <Link href={`/`}><FaGithub size={100} /></Link> 
        </div>
      </div>
    </>
  );
};

export default TechSection;
